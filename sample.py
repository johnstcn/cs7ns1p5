#!/usr/bin/env python3
import argparse
import time
import cv2
import numpy as np
import pyopencl as cl
from scipy import ndimage
import skimage.transform

def calc_fractal_numpy(chunks, maxiter):
    output_chunks = []

    for chunk_input in chunks:
      chunk_output = np.zeros(chunk_input.shape, dtype=np.uint16)

      z = np.zeros(chunk_input.shape, np.complex)

      for it in range(maxiter):
          z = z*z + chunk_input
          done = np.greater(abs(z), 2.0)
          chunk_input = np.where(done, 0+0j, chunk_input)
          z = np.where(done, 0+0j, z)
          chunk_output = np.where(done, it, chunk_output)

      output_chunks.append(chunk_output)

    return np.concatenate(output_chunks)

def calc_fractal_opencl(chunks, maxiter):
    # List all the stuff in this computer
    platforms = cl.get_platforms()

    for platform in platforms:
        print("Found a device: {}".format(str(platform)))

    # use ALL the platforms!
    props = [(cl.context_properties.PLATFORM, plt) for plt in platforms]
    ctx = cl.Context(dev_type=cl.device_type.ALL, properties=props)

    # Create a command queue on the platform (device = None means OpenCL picks a device for us)
    queue = cl.CommandQueue(ctx, device = None)

    mf = cl.mem_flags

    # This is our OpenCL kernel. It does a single point (OpenCL is responsible for mapping it across the points in a chunk)
    prg = cl.Program(ctx, """
    #pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
    __kernel void mandelbrot(__global float2 *q, __global ushort *output, ushort const maxiter)
    {
      int gid = get_global_id(0);

      float cx = q[gid].x;
      float cy = q[gid].y;

      float x = 0.0f;
      float y = 0.0f;
      ushort its = 0;

      while (((x*x + y*y) < 4.0f) && (its < maxiter)) {
        float xtemp = x*x - y*y + cx;
        y = 2*x*y + cy;
        x = xtemp;
        its++;
      }

      if (its == maxiter) {
        output[gid] = 0;
      } else {
        output[gid] = its;
      }
    }
    """).build()

    output_chunks = []
    output_chunks_on_device = []

    chunk_shape = None

    for chunk_input in chunks:
        # Record the shape of input chunks
        chunk_shape = chunk_input.shape

        # These are our buffers to hold data on the device
        chunk_input_on_device = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=chunk_input)

        chunk_output_on_device = cl.Buffer(ctx, mf.WRITE_ONLY, int(chunk_input.nbytes / 4))
        # divided by 4 because our inputs are 64 bits but outputs are 16 bits

        # Call the kernel on this chunk
        t = time.time()
        prg.mandelbrot(queue, chunk_shape, None, chunk_input_on_device, chunk_output_on_device, np.uint16(maxiter))
        print("mandelbrot {0} took {1}".format(chunk_shape, time.time()-t))

        # Add the output chunk to our list to keep track of it
        output_chunks_on_device.append(chunk_output_on_device)

    # Wait for all the chunks to be computed
    queue.finish()

    for chunk_output_on_device in output_chunks_on_device:
        t = time.time()
        chunk_output = np.zeros(chunk_shape, dtype=np.uint16)
        print("allocate output took {}".format(time.time()-t))

        # Wait until it is done and pull the data back
        t = time.time()
        cl.enqueue_copy(queue, chunk_output, chunk_output_on_device).wait()
        print("copy from device took {}".format(time.time()-t))

        # Insert the chunk in our overall output
        output_chunks.append(chunk_output)

    return np.concatenate(output_chunks)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--zoom", type=int, default=1024)
    parser.add_argument("--iterations", type=int, default=64)
    parser.add_argument("--chunks", type=int, default=10)
    parser.add_argument("--width", type=int, default=1920)
    parser.add_argument("--height", type=int, default=1080)
    parser.add_argument("--beautify", action="store_true")
    parser.add_argument("output")
    args = parser.parse_args()

    class Mandelbrot(object):
        def __init__(self):
            self.zoom = args.zoom
            self.centre_x = -(1.6*self.zoom)
            self.centre_y = 0
            self.w = args.width*4
            self.h = args.height*4
            self.w_range = args.width / 100.0
            self.h_range = args.height / 100.0
            self.iterations = args.iterations
            self.chunks = args.chunks
            self.fname = args.output
            self.beautify = args.beautify

            # To get crisper edges, also throw out points with less than this number of iterations
            self.cutoff = 0.125 * self.iterations

            self.r_spread = 0.4
            self.g_spread = 0.4
            self.b_spread = 0.4

            self.render((self.centre_x-self.w_range)/self.zoom, (self.centre_x+self.w_range)/self.zoom,
                        (self.centre_y-self.h_range)/self.zoom, (self.centre_y+self.h_range)/self.zoom, self.iterations)
            t = time.time()
            self.save_image()
            print("save image took {}".format(time.time()-t))

        def render(self, x1, x2, y1, y2, maxiter):
            # Create the input
            t = time.time()
            xx = np.arange(x1, x2, (x2-x1)/self.w)
            yy = np.arange(y2, y1, (y1-y2)/self.h) * 1j
            q = np.ravel(xx+yy[:, np.newaxis]).astype(np.complex64)
            print("allocating took {}".format(time.time()-t))

            # Slice the input up into chunks to be processed in parallel
            chunk_width = self.w
            chunk_height = self.h / self.chunks
            t = time.time()
            chunked_data = np.split(q, self.chunks)
            print("chunking took {}".format(time.time()-t))

            # Set up the output
            output = np.zeros_like(q)
            chunked_output = np.split(output, self.chunks)

            start_main = time.time()

            output = calc_fractal_opencl(chunked_data, maxiter)

            end_main = time.time()

            secs = end_main - start_main
            print("Main took", secs)

            self.mandel = output.reshape((self.h, self.w))

        def save_image(self):
            normalized = self.mandel.astype(np.double)
            normalized = (normalized / (normalized.max())) * 255.0
            normalized = np.clip(normalized - self.cutoff, 0, 255)

            b = normalized
            g = normalized
            r = normalized

            if (self.beautify):
              filtered = normalized
              b_filtered = ndimage.uniform_filter(filtered, size=int(11*self.b_spread))
              g_filtered = ndimage.uniform_filter(filtered, size=int(11*self.g_spread))
              r_filtered = ndimage.uniform_filter(filtered, size=int(11*self.r_spread))

              b_filtered_mean = b_filtered.mean()
              g_filtered_mean = g_filtered.mean()
              r_filtered_mean = r_filtered.mean()

              b = ((b_filtered / b_filtered_mean))
              g = ((g_filtered / g_filtered_mean))
              r = ((r_filtered / r_filtered_mean))

              # Renormalize
              b = (b / b.max()) * (255.0)
              g = (g / g.max()) * (255.0)
              r = (r / r.max()) * (255.0)

            cv2.imwrite(self.fname, cv2.merge((np.rint(b).astype(np.uint8),
                                               np.rint(g).astype(np.uint8),
                                               np.rint(r).astype(np.uint8))))

    # test the class
    test = Mandelbrot()
